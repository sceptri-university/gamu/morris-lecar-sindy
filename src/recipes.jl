using CairoMakie, LaTeXStrings, ColorSchemes

"""
Fetches a given amount of colors from a colormap

It is to be used with colormaps to ensure a "unified" look

## Options
- `palette` - determines whether to return a tuple ready to be used for Axis palette option
"""
function fetch_colors(colormap::Symbol, count::Integer; rev=false, palette=true, transform=false)
    color_points = count > 1 ? range(0, 1; length=count) : [0]
    if transform
        func(x) = mean([x, 0.5])
        color_points = func.(color_points)
    end
    color_points = rev ? 1 .- color_points : color_points
    colour = get(colorschemes[colormap], color_points)
    return palette ? (color=colour, patchcolor=colour) : colour
end

# ------| PLOTTING FUNCTIONS |--------
# We're NOT using @recipe macro, because it doesn't play well with Axis and stuff like that
# This is one of the areas Makie isn't really mature enough for everyday use in

AxisOrAcesVector = Union{Axis, Vector{Axis}}

"""
	dataproblemsummary!(axis::Axis, data_driven_problem::DataDrivenProblem; colormap=:Nizami, trajectory=true, labels=["V", "W"])
Helper function to make plotting the summary of a DataDrivenProblem easier

**Arguments**:
- `axis::Axis`
- `data_driven_problem::DataDrivenProblem`

**Keyword Arguments**:
- `colormap` - colormap to be used when plotting
- `trajectory` - toggle whether to plot trajectory of derivatives
- `labels` - labels for each state variable
"""
function dataproblemsummary!(axis::AxisOrAcesVector, data_driven_problem::DataDrivenProblem;
    colormap=:Nizami,
    trajectory=true,
    labels=["V", "W"]
)
    time = data_driven_problem.t
    data = trajectory ? data_driven_problem.X : data_driven_problem.DX

    palette = fetch_colors(colormap, size(data, 1); palette=false)

    for (index, row) in enumerate(eachrow(data))
		ax = typeof(axis) == Axis ? axis : axis[index]
        lines!(ax, time, row; color=palette[index], linewidth=1, label=(index <= length(labels) ? LaTeXString(labels[index]) : ""))
    end

    return axis
end

"""
	function solutionsummary!(axis::Axis, data, time; 
		colormap=:Nizami, 
		trajectory=true, 
		labels=["V", "W"]
	)

Helper function to plot data regarding solution of SINDy model
"""
function solutionsummary!(axis::Axis, data, time;
    colormap=:Nizami,
    labels=["V", "W"]
)
    palette = fetch_colors(colormap, size(data)[1]; palette=false)

    for (index, row) in enumerate(eachrow(data))
        label = index <= length(labels) ? labels[index] : ""
        lines!(axis, time, row; color=palette[index], label=LaTeXString(label))
    end

    return axis
end

"""
	function fitted_model_plot(solution::DataDrivenSolution;
		resolution=(700, 600),
		colormap=:Nizami,
		variables=(:V, :W),
		time_label=:t,
		title=L"\\text{\\textbf{Diagnostic graphs of the discovered model}}",
		position=:rt,
		legend_on_sides=true
	)

Function to plot the DataDrivenSolution (aka solution of the SINDy method). It
plots:

	| trajectories 	| estimated derivatives	|
	|---------------|-----------------------|
	| derivatives 	| errors of derivatives |

**Arguments**:
- `solution::DataDrivenSolution` - solution of the SINDy method

**Keyword Arguments**:
- `resolution` - size of the generated image
- `colormap` - colormap used for the entire image
- `time_label` - labels typically the x-axis
- `title` - title of the entire image
- `legend_on_sides` - determines, whether legend should be above plots or on sides
"""
function fitted_model_plot(solution::DataDrivenSolution;
    resolution=(700, 600),
    colormap=:Nizami,
    variables=(:V, :W),
    time_label=:t,
    title=L"\text{\textbf{Diagnostic graphs of the discovered model}}",
    legend_on_sides=true
)
    lt_plot = legend_on_sides ? (1, 1) : (2, 1)
    lt_legend = legend_on_sides ? (1, 0) : (1, 1)

    legend_move_right = pos -> (pos .+ (legend_on_sides ? (0, 3) : (0, 1)))
    legend_move_down = pos -> (pos .+ (legend_on_sides ? (1, 0) : (3, 0)))
    legend_move_diag = pos -> legend_move_right(legend_move_down(pos))

    plot_move_right = pos -> (pos .+ (0, 1))
    plot_move_down = pos -> (pos .+ (1, 0))
    plot_move_diag = pos -> plot_move_right(plot_move_down(pos))

    system = get_basis(solution)
    parameters = get_parameter_map(system)
    problem = get_problem(solution)

    figure = Figure(; resolution)
    axis1 = Axis(figure[lt_plot...];
        xlabel=LaTeXString(to_string(time_label)),
        ylabel=LaTeXString(to_string(variables))
    )
    dataproblemsummary!(axis1, problem; colormap, labels=to_string(variables, format_to="%s(t)", do_join=false))
    Legend(figure[lt_legend...], axis1; tellheight=!legend_on_sides, tellwidth=legend_on_sides)

    axis2 = Axis(figure[plot_move_down(lt_plot)...],
        xlabel=LaTeXString(to_string(time_label)),
        ylabel=LaTeXString(to_string(variables; format_to="\\dot{%s}"))
    )
    dataproblemsummary!(axis2, problem;
        trajectory=false,
        colormap,
        labels=to_string(variables; format_to="\\dot{%s}(t)", do_join=false)
    )
    Legend(figure[legend_move_down(lt_legend)...], axis2; tellheight=!legend_on_sides, tellwidth=legend_on_sides)

    fitted_equations = [eq.rhs for eq in equations(system)]
    calc_derivatives = similar(problem.DX)
    for (eq_index, equation) in enumerate(fitted_equations)
        equation_variables = get_variables(equation)
        present_variables = [var for var in variables if var in Symbol.(equation_variables)]

        for (col_index, column) in enumerate(eachcol(problem.X))
            variable_indices = [findfirst(var -> var == variable, Symbol.(equation_variables)) for variable in present_variables]
            states = Dict(equation_variables[variable_indices[index]] => column[index] for (index, variable) in enumerate(present_variables))

            calc_derivatives[eq_index, col_index] = substitute(fitted_equations[eq_index], Dict(parameters..., states...))
        end
    end

    axis3 = Axis(figure[plot_move_right(lt_plot)...],
        xlabel=LaTeXString(to_string(time_label)),
        ylabel=LaTeXString(to_string(variables; format_to="\\frac{\\mathrm{d}}{\\mathrm{d}\\,t}\\hat{%s}"))
    )
    solutionsummary!(axis3, calc_derivatives, problem.t; colormap, labels=to_string(variables, format_to="\\frac{\\mathrm{d}}{\\mathrm{d}\\,t}\\hat{%s}(t)", do_join=false))
    Legend(figure[legend_move_right(lt_legend)...], axis3; tellheight=!legend_on_sides, tellwidth=legend_on_sides)

    axis4 = Axis(figure[plot_move_diag(lt_plot)...],
        xlabel=LaTeXString(to_string(time_label)),
        ylabel=LaTeXString(to_string(variables; format_to="e_{%s}"))
    )
    solutionsummary!(axis4, problem.DX .- calc_derivatives, problem.t; colormap, labels=to_string(variables, format_to="e_{%s}(t)", do_join=false))
    Legend(figure[legend_move_diag(lt_legend)...], axis4; tellheight=!legend_on_sides, tellwidth=legend_on_sides)

    Label(figure[0, :], title)

    figure
end


"""
	function predicted_trajectory_comparison_plot(time, predicted, truth, variables;
		resolution=(600, 450),
		time_label=:t,
		title=L"\\text{\\textbf{Predicted trajectory}}"
	)

Plots the comparison of predicted trajectories - all in one axis

**Arguments**:
- `time` - vector of time measurements
- `predicted` - trajectory predicted by discovered SINDy method
- `truth` - true trajectory
- `variables` - variable names used for plotting

**Keyword Arguments**:
- `resolution` - size of the generated image
- `colormap` - colormap used for the entire image
- `time_label` - labels typically the x-axis
- `title` - title of the entire image
"""
function predicted_trajectory_comparison_plot(time, predicted, truth, variables;
    resolution=(600, 450),
    time_label=:t,
    title=L"\text{\textbf{Predicted trajectory}}",
    colormap=:Nizami
)
    figure = Figure(; resolution)
    axis = Axis(figure[1, 1];
        title,
        xlabel=LaTeXString(to_string(time_label)),
        ylabel=LaTeXString(to_string(variables)),
        palette=fetch_colors(colormap, length(variables))
    )

    for (variable_index, variable_name) in enumerate(variables)
        lines!(axis, time, predicted'[:, variable_index], label=L"\text{predicted } %$variable_name", linewidth=1, color=Cycled(variable_index))
        lines!(axis, time, truth'[:, variable_index], label=L"\text{true } %$variable_name", linestyle=:dot, color=Cycled(variable_index))
    end

    Legend(figure[1, 2], axis)
    figure
end


"""
	function dataproblem_plot(problem::T;
		resolution=(700, 600),
		colormap=:Nizami,
		variables=(:V, :W),
		time_label=:t,
		title=L"\\text{\\textbf{Data problem summary}}",
		position=:rt
	) where {T<:DataDrivenProblem}

User facing function to plot complete summary of DataDrivenProblem, using helper function `dataproblemsummary`

**Arguments:**
- `problem::DataDrivenProblem`

**Keyword Arguments**:
- `resolution` - size of the generated image
- `colormap` - colormap used for the entire image
- `variables` - variable names used legends
- `time_label` - labels typically the x-axis
- `title` - title of the entire image
- `position` - position of the legend (possible values are `:rt`, `:rb`, `:lt`, `:lb` and maybe more)
"""
function dataproblem_plot(problem::T;
    resolution=(700, 600),
    colormap=:Nizami,
    variables=(:V, :W),
    time_label=:t,
    title=L"\text{\textbf{Data problem summary}}",
    position=:rt,
	do_each = false
) where {T<:DataDrivenProblem}
    figure = Figure(; resolution)

    ylabel = do_each ?
        @.(LaTeXString(to_string(variables))) :
        LaTeXString(to_string(variables))

    top_axis = Axis(figure[1, 1];
        xlabel=LaTeXString(to_string(time_label)),
        ylabel=do_each ? ylabel[1] : ylabel,
        title=do_each ? "" : title
    )
	if do_each
		second_top_axis = Axis(figure[1, 2];
			xlabel=LaTeXString(to_string(time_label)),
			ylabel=ylabel[2]
		)
	end

	if do_each
        Label(figure[0, :], title)
	end

    dataproblemsummary!(do_each ? [top_axis, second_top_axis] : top_axis, problem; colormap, labels=to_string(variables, format_to="%s(t)", do_join=false))
    axislegend(top_axis; position)
    do_each && axislegend(second_top_axis; position)

	ylabel = do_each ? 
	@.(LaTeXString(to_string(variables; format_to="\\dot{%s}"))) :
	LaTeXString(to_string(variables; format_to="\\dot{%s}"))

    bottom_axis = Axis(figure[2, 1],
        xlabel=LaTeXString(to_string(time_label)),
        ylabel=do_each ? ylabel[1] : ylabel
    )
    if do_each
        second_bottom_axis = Axis(figure[2, 2];
            xlabel=LaTeXString(to_string(time_label)),
            ylabel=ylabel[2]
        )
    end
    dataproblemsummary!(do_each ? [bottom_axis, second_bottom_axis] : bottom_axis, problem;
        trajectory=false,
        colormap,
        labels=to_string(variables; format_to="\\dot{%s}(t)", do_join=false)
    )

    axislegend(bottom_axis; position)
    do_each && axislegend(second_bottom_axis; position)
    figure
end


"""
	function each_predicted_trajectory_comparison_plot(time, predicted, truth, variables;
		resolution=(600, 450),
		time_label=:t,
		title=L"\\text{\\textbf{Predicted trajectory}}",
		position=:rb,
		colormap=:Nizami,
		scale_limits=false
	)

Plots the comparison of predicted trajectories - each in a separate axis

**Arguments**:
- `time` - vector of time measurements
- `predicted` - trajectory predicted by discovered SINDy method
- `truth` - true trajectory
- `variables` - variable names used for plotting

**Keyword Arguments**:
- `resolution` - size of the generated image
- `colormap` - colormap used for the entire image
- `time_label` - labels typically the x-axis
- `title` - title of the entire image
- `position` - position of the legend (possible values are `:rt`, `:rb`, `:lt`, `:lb` and maybe more)
- `scale_limits` - rescales limits of the graph if necessary
"""
function each_predicted_trajectory_comparison_plot(time, predicted, truth, variables;
    resolution=(600, 450),
    time_label=:t,
    title=L"\text{\textbf{Predicted trajectory}}",
    position=:rb,
    colormap=:Nizami,
    scale_limits=false,
	adjust_length=true,
	observables=false,
	predicted_frequencies=undef,
	true_frequencies=undef
)
    figure = Figure(; resolution)
	axes = []

	freq_defined = true_frequencies != undef && predicted_frequencies != undef

    for (variable_index, variable_name) in enumerate(variables)
        xlabel = to_string(time_label) * sprintf(", true \$f_t=%2.3f\$ Hz, predicted \$f_p=%2.3f\$ Hz",
			true_frequencies[variable_index], predicted_frequencies[variable_index])

		xlabel = freq_defined ? LaTeXString(xlabel) : (
			variable_index == length(variables) ? LaTeXString(to_string(time_label)) : ""
		)

        axis = Axis(figure[variable_index, 1];
            title=variable_index == 1 ? title : "",
            xlabel,
            ylabel=LaTeXString(to_string(variable_name)),
            palette=fetch_colors(colormap, length(variables))
        )
		push!(axes, axis)

        current_truth = copy(truth)
		current_prediction = copy(predicted)
		current_time = copy(time)

		if adjust_length
			min_length = min(size(truth, 2), size(predicted, 2))
			
			current_time = current_time[1:min_length]
			current_truth = current_truth'[1:min_length, :]
            current_prediction = current_prediction'[1:min_length, :]
		end

        current_truth = observables ? 
			current_truth[variable_index] : current_truth[:, variable_index]
		current_prediction = observables ?
			current_prediction[variable_index] : current_prediction[:, variable_index]

        lines!(axis, current_time, current_truth, label=L"\text{true } %$variable_name", color=Cycled(2))
        if scale_limits
            autolimits!(axis)
            correct_limits = axis.finallimits[]
        end

        lines!(axis, current_time, current_prediction, label=L"\text{predicted } %$variable_name", color=Cycled(1))

        if scale_limits
            limits!(axis, correct_limits)
        end

        Legend(figure[variable_index, 2], axis)
    end

    figure, axes
end

function plot_found_phase_space(transformed_trajectory, native_trajectory,
    x_bounds, y_bounds, f, f₁_space, f₂_space, V_space, n_space;
    resolution=(350, 400),
    colormap=:Nizami,
    title=L"\text{\textbf{Linear transformation attractor}}",
    xlabel=L"V",
    ylabel=L"W",
    position=:rt,
    legend_outside=false,
    stepsize=0.05,
    arrow_size=8,
    maxsteps=40,
    shift_colors=false,
    show_legend=true,
    show_title=true,
    native_label=L"\text{native trajectory}",
    transform_label=L"\text{transformed trajectory}",
	contour_colors = [:cyan, :teal, :green, :darkgreen],
	level=1e-5,
	steps=300
)
    figure = Figure(; resolution)
    axis = Axis(figure[1, 1];
        xlabel,
        ylabel,
        palette=fetch_colors(colormap, 2; rev=true),
        title=show_title ? title : ""
    )
    colors = fetch_colors(colormap, shift_colors ? 3 : 2; palette=false)

    streamplot!(axis, f, x_bounds, y_bounds; stepsize, arrow_size, maxsteps, colormap)

    r₁ = range(minimum(x_bounds), stop=maximum(x_bounds), length=steps)
    r₂ = range(minimum(y_bounds), stop=maximum(y_bounds), length=steps)

    contour!(axis, r₁, r₂, f₁_space; levels=[level],
        linewidth=2, color=contour_colors[1])
    contour!(axis, r₁, r₂, f₂_space; levels=[level],
        linewidth=2, color=contour_colors[2])
    contour!(axis, r₁, r₂, V_space; levels=[level],
        linewidth=2, color=contour_colors[3])
    contour!(axis, r₁, r₂, n_space; levels=[level],
        linewidth=2, color=contour_colors[4])

    lines!(axis, native_trajectory,
        linewidth=3,
        label=native_label,
        color=colors[shift_colors ? 2 : 1]
    )

    lines!(axis, transformed_trajectory,
        linewidth=3,
        label=transform_label,
        color=colors[end]
    )

	labels = [
		native_label,
		transform_label,
		L"\text{polynomial } x-\text{nullcline}",
		L"\text{polynomial } y-\text{nullcline}",
        L"\text{ML } x-\text{nullcline}",
        L"\text{ML } y-\text{nullcline}"
	]

	elements = [
        LineElement(color=colors[shift_colors ? 2 : 1], linestyle = nothing, linewidth = 3),
        LineElement(color=colors[end], linestyle = nothing, linewidth = 3),
        LineElement(color=contour_colors[1], linestyle = nothing, linewidth = 2),
        LineElement(color=contour_colors[2], linestyle = nothing, linewidth = 2),
        LineElement(color=contour_colors[3], linestyle = nothing, linewidth = 2),
        LineElement(color=contour_colors[4], linestyle = nothing, linewidth = 2)
	]

    if legend_outside && show_legend
        Legend(figure[2, 1], elements, labels; tellwidth=false, tellheight=true)
    elseif show_legend
        axislegend(; position)
    end

    figure, axis
end