function check_same_size(iterators)
    referential_length = length(first(iterators))
    return all(length.(iterators) .== referential_length)
end

struct CoupledIterators
	iterators

	function CoupledIterators(iterators...)
        #@assert check_same_size(iterators) "All iterators must be of the same length"
		return new(iterators)
	end

end

function Base.iterate(couple::CoupledIterators; kwargs...)
    if isempty(first(couple.iterators))
        return nothing
    end

	iteration, states = [], []
	ready_for_end = true
	for iterator in couple.iterators
		response = iterate(iterator)

        ready_for_end = ready_for_end && isnothing(response)
		if isnothing(response)
			continue
		end

        value, state = response
		push!(iteration, value)
		push!(states, state)
	end

	if ready_for_end
		return nothing
	end

    return iteration, states
end

function Base.iterate(couple::CoupledIterators, states; kwargs...)
    if isempty(first(couple.iterators))
        return nothing
    end

    iteration, new_states = [], []
    ready_for_end = true
    for (index, iterator) in enumerate(couple.iterators)
		if isnothing(states[index])
			continue
		end

        response = iterate(iterator, states[index])

        ready_for_end = ready_for_end && isnothing(response)
        if isnothing(response)
            continue
        end

        value, state = response
        push!(iteration, value)
        push!(new_states, state)
    end

    if ready_for_end
        return nothing
    end

    return iteration, new_states
end

"""
	function animate(plot_function, filename, iterated_args...;
		temporary_folder = "temp",
		temporary_filename = "frame",
		filetype = "png",
		px_per_unit=3.0,
		framerate=2
	)

Naive implementation of an animate function, which relies on `convert`
being installed locally on the machine to convert generated images into a desired format

> Use this if and only if Observables don't work

Length of all iterators **should** be the same, but it is not checked for performance reasons
"""
function animate(plot_function, filename, iterated_args...;
    temporary_folder="temp",
    temporary_filename="frame",
    filetype="png",
    px_per_unit=3.0,
    framerate=2,
	kwargs...
)
    #@assert check_same_size(iterated_args) "All iterators must be of the same length"
	argument_iterator = CoupledIterators(iterated_args...)

    for (index, current_iteration) in enumerate(argument_iterator)
        @show index
		

        fig = plot_function(current_iteration..., kwargs)
        safesave(plotsdir(temporary_folder, temporary_filename * "." * filetype), fig; px_per_unit)
    end

    from_filename = plotsdir(temporary_folder, temporary_filename)

    delay = round(60 / framerate)

    command = `convert -delay $delay -loop 0 $from_filename\*.$filetype $filename`
    run(command)

    rm(plotsdir(temporary_folder); recursive=true)
end