using Printf, LaTeXStrings

# Taken from: https://discourse.julialang.org/t/how-to-track-total-memory-usage-of-julia-process-over-time/91167/5
function meminfo_julia()
    # @printf "GC total:  %9.3f MiB\n" Base.gc_total_bytes(Base.gc_num())/2^20
    # Total bytes (above) usually underreports, thus I suggest using live bytes (below)
    @printf "GC live:   %9.3f MiB\n" Base.gc_live_bytes() / 2^20
    @printf "JIT:       %9.3f MiB\n" Base.jit_total_bytes() / 2^20
    @printf "Max. RSS:  %9.3f MiB\n" Sys.maxrss() / 2^20
end


function meminfo_procfs(pid=getpid())
    smaps = "/proc/$pid/smaps_rollup"
    if !isfile(smaps)
        error("`$smaps` not found. Maybe you are using an OS without procfs support or with an old kernel.")
    end

    rss = pss = shared = private = 0
    for line in eachline(smaps)
        s = split(line)
        if s[1] == "Rss:"
            rss += parse(Int64, s[2])
        elseif s[1] == "Pss:"
            pss += parse(Int64, s[2])
        elseif s[1] == "Shared_Clean:" || s[1] == "Shared_Dirty:"
            shared += parse(Int64, s[2])
        elseif s[1] == "Private_Clean:" || s[1] == "Private_Dirty:"
            private += parse(Int64, s[2])
        end
    end

    @printf "RSS:       %9.3f MiB\n" rss / 2^10
    @printf "┝ shared:  %9.3f MiB\n" shared / 2^10
    @printf "┕ private: %9.3f MiB\n" private / 2^10
    @printf "PSS:       %9.3f MiB\n" pss / 2^10
end

parameters_values(params) = [last(param) for param in params]

to_matrix(trajectory) = vcat(reshape.(trajectory, 1, length(trajectory[end]))...)

calc_derivatives(derivatives, trajectory, parameters) = [variable_derivative(state..., parameters...) for state in eachrow(trajectory), variable_derivative in derivatives]

import Base.show
import DataDrivenDiffEq: DataDrivenSolution, Basis

function show(io::IO, whole_solution::Tuple{DataDrivenSolution,Basis,Vector})
    solution, system, parameters = whole_solution

    println(io, system)
    println(io, "")
    println(io, solution)
    println(io, "")
    println(io, "Parameters:")
    println(io, parameters)
end

sprintf(format, values...) = Printf.format(Printf.Format(format), values...)

function to_string(strings::AbstractVector{String}; format_to="%s", join_by=',', do_join=true)
    formatted = [("\$" * sprintf(format_to, one_string) * "\$") for one_string in strings]
    return do_join ? (join(formatted, join_by)) : formatted
end

to_string(symbols::AbstractVector{Symbol}; kwargs...) = to_string([String(symbol) for symbol in symbols]; kwargs...)
to_string(symbol::Symbol; kwargs...) = to_string([symbol]; kwargs...)
to_string(one_string::String; kwargs...) = to_string([one_string]; kwargs...)
to_string(tuple::Tuple; kwargs...) = to_string(collect(tuple); kwargs...)
to_string(string::AbstractString; more...) = string
to_string(string::LaTeXString; more...) = string

import ModelingToolkit: ODESystem

function system_to_functions(system::ODESystem, system_variables, parameter_map)
    @assert length(system_variables) == length(system.eqs) "Same number of input variables and equations in system must be supplied"

    rhs_functions = []

    for equation in system.eqs
        args_function = substitute(equation.rhs, parameter_map)
        func_expr = build_function(args_function, system_variables...)

        push!(rhs_functions, eval(func_expr))
    end

    return (vec...) -> [rhs_function(vec...) for rhs_function in rhs_functions]
end

struct Experiment{T<:Number}
    trajectory::Matrix{T}
    derivatives::Matrix{T}
    time_ticks::Vector{T}
    C_value::T
    Iext_value::T
    initial_condition::Vector{T}
end

function collect_data(experiments::Vector{Experiment{T}}) where {T<:Number}
    concat = field -> vcat([getproperty(exp, field) for exp in experiments]...)

    trajectory, derivatives, time_ticks = concat.([:trajectory, :derivatives, :time_ticks])
    C_trajectory = vcat([experiment.C_value .* ones(size(experiment.time_ticks)) for experiment in experiments]...)
    Iext_trajectory = vcat([experiment.Iext_value .* ones(size(experiment.time_ticks)) for experiment in experiments]...)

    return hcat(trajectory, C_trajectory, Iext_trajectory),
    hcat(derivatives, zeros(size(C_trajectory)), zeros(size(Iext_trajectory))),
    time_ticks
end

import DataDrivenDiffEq: DataDrivenProblem

function collect_data_to_problems(experiments::Vector{Experiment{T}}) where {T<:Number}
    problems = Vector{NamedTuple}(undef, length(experiments))

    for (index, experiment) in enumerate(experiments)
        problem = (X=experiment.trajectory',
            t=experiment.time_ticks,
            DX=experiment.derivatives',
            p=[experiment.C_value, experiment.Iext_value]
        )

        problems[index] = problem
    end

    keys = "problem_" .* string.(1:length(problems)) .|> Symbol

    return (; zip(keys, problems)...)
end

function calculate_value_space(func, x_bounds, y_bounds;
    steps=300
)
    r₁ = range(minimum(x_bounds), stop=maximum(x_bounds), length=steps)
    r₂ = range(minimum(y_bounds), stop=maximum(y_bounds), length=steps)

    f₁ = (x, y) -> func(x, y)[1]
    f₂ = (x, y) -> func(x, y)[2]

    return f₁.(r₁, r₂'), f₂.(r₁, r₂')
end

differential_to_string(diff) = string(diff)[end-1] |> string
remove_unicode(message) = replace(message, ['₀' + i => i for i in 0:9]...)
make_list(array; by=',') = join(array, by)

function represent_as_text(system, initial_condition;
    lhs_format="%s'",
    time="t",
    equals="="
)
    variables = [equation.lhs for equation in system.eqs] .|> differential_to_string

    parameter_names = string.(parameters(system))
    parameter_values = join(string.(get_parameter_map(system)), '\n')

    equations_rhs = [string(equation.rhs) for equation in system.eqs]
    equations = [sprintf(lhs_format, variables[index]) * equals * equations_rhs[index]
                 for index in eachindex(equations_rhs)]

    lined_equations = join(equations, '\n')

    starting_variables = join([variables[index] * equals * string(initial_condition[index])
                               for index in eachindex(variables)], '\n')


    composed_text = """
    -- System definition:
    Coordinates=$(make_list(variables))
    Parameters=$(make_list(parameter_names))
    Time=$time

    -- Equations:
    $lined_equations

    -- Values:
    $starting_variables
    $parameter_values
    """

    return remove_unicode(composed_text)
end

import FileIO.save
function save(filename, data::String)
    open(filename, "w+") do io
        println(io, data)
    end
end

function dominant_frequency(signal, sampling_rate)
    signal_step_diference = signal[2:end] .- signal[1:end-1]
    # Find where the signal is incresing, resp. decreasing
    signal_ups = signal_step_diference .> 0
    signal_downs = signal_step_diference .< 0

    # Find where the monotony changes from up to down
    signal_monotony = signal_ups[2:end] .&& signal_downs[1:end-1]

    # Compute mean distance between ups
    ups = findall(identity, signal_monotony)
    avg_distance = mean(ups[2:end] .- ups[1:end-1])

    return avg_distance ≈ 0 ? NaN : sampling_rate / avg_distance
end

function to_ODE_system(fitted_system; name=:fitted_system)
    return ODESystem(
        equations(fitted_system),
        get_iv(fitted_system),
        states(fitted_system),
        parameters(fitted_system);
        checks=false,
        name
    )
end

function ensure_dims(matrices...; trim_dim=1)
    dim_length = minimum(size.(matrices, trim_dim))

    return (getindex(matrix, 1:dim_length, :) for matrix in matrices)
end

function map_to_results(
    fitted_system,
    fitted_predictive_system,
    data_driven_dataset,
    sim_results
)
    return map(eachindex(sim_results["experiments"])) do use_scenario
        @show use_scenario
        data_driven_problem = data_driven_dataset.probs[use_scenario]

        # For each sample define starting values, timespan and parameters
        starting_values = data_driven_problem.X[:, 1]
        timespan = (data_driven_problem.t[begin], data_driven_problem.t[end])
        fitted_parameters = get_parameter_map(fitted_system)

        # Manually insert C and Iext parameters' values 
        for i in [1, 2]
            fitted_parameters[i] = parameters(fitted_system)[i] => parameters(data_driven_problem)[i]
        end

        # Computing the prediction for each sample
        ode_predictive_problem = ODEProblem(fitted_predictive_system, starting_values, timespan, fitted_parameters)
        prediction = solve(ode_predictive_problem, Rosenbrock23(); saveat=data_driven_problem.t)
        prediction_trajectory = to_matrix(prediction.u)

        prediction_trajectory, original_trajectory = ensure_dims(prediction_trajectory, data_driven_problem.X')

        rss = sum(abs2, original_trajectory - prediction_trajectory)

        # Make fitted system callable
        fitted_functions = system_to_functions(fitted_predictive_system, [V, n], fitted_parameters)
        point2_functions = (V, n) -> Point2f(fitted_functions(V, n))

        # Compute dominant frequency for each sample
        sampling_rate = 1 / (data_driven_problem.t[2] - data_driven_problem.t[1])
        frequencies = [dominant_frequency(var_trajectory, sampling_rate) for var_trajectory in eachcol(prediction_trajectory)]
        ML_frequencies = [dominant_frequency(var_trajectory, sampling_rate) for var_trajectory in eachrow(data_driven_problem.X)]

        # Precomputing parameters
        ML_params = [parameters(data_driven_problem)[1], parameters_values(sim_results["default_params"])..., parameters(data_driven_problem)[2]]

        # Return tuple of useful precomputed info
        return (
            # Trajectories
            prediction=prediction_trajectory,
            original=original_trajectory,

            # Functions for streamplots
            functions=point2_functions,
            ML_functions=(V, n) -> Point2f([
                V̇(V, n, ML_params...),
                ṅ(V, n, ML_params...)
            ]),

            # Parameter values
            C_value=parameters(data_driven_problem)[1],
            Iext_value=parameters(data_driven_problem)[2],

            # Frequencies
            frequencies=frequencies,
            ML_frequencies=ML_frequencies,

            # Miscellaneous
            time_ticks=prediction.t,
            rss=rss
        )
    end
end