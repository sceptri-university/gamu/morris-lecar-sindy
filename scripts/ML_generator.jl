using DrWatson
@quickactivate "Morris-Lecar SINDy"

using Revise, Random
using ModelingToolkit, DifferentialEquations, DiffEqCallbacks
using CairoMakie, LaTeXStrings

Random.seed!(0)

@variables t, V(t), n(t)
@parameters C, gₖ, Vₖ, gₗ, Vₗ, gₐ, Vₐ, β₁, β₂, β₃, β₄, φ, Iₑ

# Morris-Lecar is an ODE, so differential with respect to time suffices
D = Differential(t)

# Define parameter values and ICs for which we want to compute the trajectory
C_values = 0.7:0.1:1.3
Iext_values = [36.0, 38.0, 41.0, 43.0, 45.0]
V_range = [-50.0, 50.0]
n_range = 0:0.1:1

default_params = [
    gₖ => 8,
    Vₖ => -80,
    gₗ => 2,
    Vₗ => -60,
    gₐ => 4,
    Vₐ => 120,
    β₁ => -1.2,
    β₂ => 18,
    β₃ => 10,
    β₄ => 17.4,
    φ => 1 / 15,
]

params_hash = hash(default_params)

time_range = (0.0, 1000.0)
saveat = 0.5
# cut off at time 100 (which is transformed to timesteps)
cycle_cutoff = round(Int, 100 / saveat)

# Include the equations of Morris-Lecar model
include(srcdir("models", "morris-lecar.jl"))
include(srcdir("utils.jl"))

@named morris_lecar_system = ODESystem([
    D(V) ~ V̇(V, n, C, gₖ, Vₖ, gₗ, Vₗ, gₐ, Vₐ, β₁, β₂, β₃, β₄, φ, Iₑ),
    D(n) ~ ṅ(V, n, C, gₖ, Vₖ, gₗ, Vₗ, gₐ, Vₐ, β₁, β₂, β₃, β₄, φ, Iₑ)
])

experiment_count = length(C_values) * length(Iext_values) *
                   length(V_range) * length(n_range)

# Define a structure for one iteration
experiments = Vector{Experiment{Float64}}(undef, experiment_count)

index = 1

# C_param = C_values[2]
# Iext_param = Iext_values[4]
# V_value = V_range[2]
# n_value = n_range[2]

for C_param in C_values, Iext_param in Iext_values,
    V_value in V_range, n_value in n_range

    @show index
    params = [C => C_param, copy(default_params)..., Iₑ => Iext_param]

    initial_condition = [V_value, n_value]
    @named morris_lecar_problem = ODEProblem(morris_lecar_system, initial_condition, time_range, params)

    saveat_span = time_range[1]:saveat:time_range[end]

    derivatives = SavedValues(Float64, Vector{Float64})
    diff_callback = SavingCallback((u, t, integrator) -> copy(get_du(integrator)), derivatives; saveat=saveat_span)

    solution = solve(morris_lecar_problem, Rosenbrock23(); saveat, callback=diff_callback)
    trajectory = to_matrix(solution.u)
    derivatives = to_matrix(derivatives.saveval)

    cycle = trajectory[cycle_cutoff:end, :]
    cycle_derivatives = derivatives[cycle_cutoff:end, :]
    time_ticks = solution.t[cycle_cutoff:end]

    experiment = Experiment(
        cycle, cycle_derivatives, time_ticks,
        C_param, Iext_param,
        initial_condition
    )
    experiments[index] = experiment
    index += 1
end

parameter_hash = hash((C=C_values, Iₑ=Iext_values, V=V_range, n=n_range))

sim_params = @dict time_range saveat cycle_cutoff C_values Iext_values V_range n_range parameter_hash
sim_results = @dict experiments sim_params default_params

@tagsave(datadir("sims", "ML_cycles", savename(sim_params, "jld2")), sim_results; safe=true)
