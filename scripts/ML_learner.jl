using DrWatson
@quickactivate "Morris-Lecar SINDy"

using Revise
using DataDrivenDiffEq, DataDrivenSparse

include(srcdir("Sparse", "Sparse.jl"))
using .Sparse # my custom module

using ModelingToolkit, DifferentialEquations, Statistics
using CairoMakie, ColorSchemes, LaTeXStrings

# Sparse module contains DSTLS optimizer
include(srcdir("utils.jl"))
include(srcdir("recipes.jl"))

parameter_hash = "3794102255037306175"
sim_results = load(datadir("sims", "ML_cycle", parameter_hash * "_C_param=1.0_cycle_cutoff=100_saveat=0.1.jld2"))

optimizer_threshold = 1e-5
ridge_parameter = 2
library_power = 6
optimizer_name = "STLSQ"

learning_params = @dict optimizer_threshold ridge_parameter library_power optimizer_name parameter_hash

trajectory = sim_results["cycle"]
derivatives = sim_results["derivatives"]
time = sim_results["time_ticks"]

@named data_driven_problem = ContinuousDataDrivenProblem(trajectory', time, derivatives')

dataproblem_plot(data_driven_problem; 
	variables=[:V, :n], position=:rt, do_each = true,
    resolution=(600, 500))

@variables V, n

terms_library = polynomial_basis([V, n], library_power)
@named model_basis = Basis(terms_library, [V, n])

optimizer = STLSQ(optimizer_threshold, ridge_parameter)
@assert occursin(optimizer_name, string(optimizer)) "Optimizer name and the actual optimizer must correspond"

fitted_solution = solve(data_driven_problem, model_basis, optimizer)
fitted_system = get_basis(fitted_solution)

@show aic(fitted_solution), r2(fitted_solution)
@show length(fitted_system.eqs)

fig = fitted_model_plot(fitted_solution; colormap=:Nizami, variables=[:V, :n])
display(fig)
safesave(plotsdir("learner", "fitted_model_plot", savename(learning_params, "pdf")), fig)

# Creating an ODE System from our fitted model
@named fitted_predictive_system = ODESystem(
    equations(fitted_system),
    get_iv(fitted_system),
    states(fitted_system),
    parameters(fitted_system);
    checks=false
)

# Graphing true trajectory, noisy trajectory and our prediction based on fitted model for each experiment
# Defining useful values for generating the prediction
starting_values = data_driven_problem.X[:, 1]
parameter_values = get_parameter_map(fitted_system)
timespan = (data_driven_problem.t[begin], data_driven_problem.t[end])

# Computing the prediction
ode_predictive_problem = ODEProblem(fitted_predictive_system, starting_values, timespan, parameter_values)
prediction = solve(ode_predictive_problem, Rosenbrock23(); saveat=data_driven_problem.t)
prediction_trajectory = to_matrix(prediction.u)

fig = each_predicted_trajectory_comparison_plot(data_driven_problem.t,
	prediction_trajectory', trajectory', [:V, :n];
	resolution=(550, 350))
display(fig)
safesave(plotsdir("learner", "each_predicted_trajectory_comparison_plot", savename(learning_params, "pdf")), fig)

fitted_functions = system_to_functions(fitted_predictive_system, [V,n], get_parameter_map(fitted_system))

fig = plot_found_phase_space(trajectory, prediction_trajectory, 
	-70..70, -0.1..0.4, (V,n) -> Point2f(fitted_functions(V,n));
	legend_outside = true, maxsteps = 60,
	native_label=L"\text{polynomial trajectory}",
    transform_label=L"\text{Morris-Lecar trajectory}",
	title=L"\text{\textbf{Polynomial phase space}}")
display(fig)
safesave(plotsdir("learner", "plot_found_phase_space", savename(learning_params, "pdf")), fig)

