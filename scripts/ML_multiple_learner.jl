# Clone of ML_learner.jl file that learns 
# from multiple trajectories with differing C values

using DrWatson
@quickactivate "Morris-Lecar SINDy"

using Revise
using DataDrivenDiffEq, DataDrivenSparse

include(srcdir("Sparse", "Sparse.jl"))
using .Sparse # my custom module

using ModelingToolkit, DifferentialEquations
using CairoMakie, ColorSchemes, LaTeXStrings
using Statistics, StableRNGs, Random

# Sparse module contains DSTLS optimizer
include(srcdir("utils.jl"))
include(srcdir("recipes.jl"))

# Choose dataset to take learning data from
dataset_name = "cycle_cutoff=200_parameter_hash=15917758132858491632_saveat=0.5.jld2"
dataset_hash = hash(dataset_name)

sim_results = load(datadir("sims", "ML_cycles", dataset_name))

# Define the parameters of the learning process
optimizer_threshold = 1e-5
ridge_parameter = 1e3
library_power = 9
optimizer_name = "DSTLS"
# sampler_batchsize = 100
# sampler_split = 0.8
# sampler_digits = 10
rng_seed = 123

rng = StableRNG(rng_seed)
Random.seed!(rng_seed)

learning_params = @dict optimizer_threshold ridge_parameter library_power optimizer_name dataset_hash rng_seed# sampler_batchsize sampler_digits sampler_split

# Define a dataset SINDy problem
@time(@named data_driven_dataset = ContinuousDataset(collect_data_to_problems(sim_results["experiments"])))

@variables V n
@parameters C Iext t

# Choose libraries of candidate terms
terms_library = polynomial_basis([V, n], library_power)
@named model_basis = Basis(1 / C .* (terms_library ∪ [Iext]), [V, n]; parameters=[C, Iext], iv=t)

# And finally choose optimizer (it's name must match that from the params)
optimizer = DSTLS(optimizer_threshold, ridge_parameter)
@assert occursin(optimizer_name, string(optimizer)) "Optimizer name and the actual optimizer must correspond"

# Define how we preprocess data
# sampler = DataProcessing(; split=sampler_split,
#     shuffle=true,
#     batchsize=sampler_batchsize,
#     rng
# )

# Solve the SINDy problem given our chosen library and optimizer and data processer
@time fitted_solution = solve(data_driven_dataset, model_basis, optimizer)
print(fitted_solution)

fitted_system = get_basis(fitted_solution)

# Show useful diagnostics
#@show aic(fitted_solution), r2(fitted_solution), dof(fitted_solution)
@assert length(fitted_system.eqs) == 2 "Less than 2 nonzero equations were found in the system. Aborting!"

# Creating an ODE System from our fitted model
fitted_predictive_system = to_ODE_system(fitted_system)

# Function definitions for Morris-Lecar model
include(srcdir("models", "morris-lecar.jl"))

# Convert fitted system into text file usable with MatCont
initial_condition = data_driven_dataset.probs[1].X[:, 1]
safesave(datadir("systems", savename(learning_params, "txt")), represent_as_text(fitted_system, initial_condition))

# Precompute useful data, e.g.
#	- trajectory predictions for each parameter/IC sample
#	- dominant frequencies, callable functions, etc.
@time results_for_Cs = map_to_results(
    fitted_system,
    fitted_predictive_system,
    data_driven_dataset,
    sim_results
);

# Display the graph of RSSs over with respect to the parameter/IC pairs
lines(getindex.(results_for_Cs, :rss))

mean(size.(getindex.(results_for_Cs, :original), 1))
results_for_Cs[1].time_ticks

original_trajectories = [Observable([row...]) for row in eachrow(data_driven_dataset.probs[1].X)]
time_ticks = Observable(data_driven_dataset.probs[1].t)
predicted_trajectories = [Observable([col...]) for col in eachcol(results_for_Cs[1].prediction)]
phase_space_function = Observable(results_for_Cs[1].functions)

C_value = data_driven_dataset.probs[1].p[1]
Iext_value = data_driven_dataset.probs[1].p[2]

fig, axes = each_predicted_trajectory_comparison_plot(time_ticks,
    predicted_trajectories, original_trajectories, [:V, :n];
    resolution=(550, 350), adjust_length=false, observables=true,
    title=latexstring("\\text{\\textbf{Predicted trajectory}}, C = " * string(C_value) * ", I_e = " * string(Iext_value)),
    predicted_frequencies=results_for_Cs[1].frequencies,
    true_frequencies=results_for_Cs[1].ML_frequencies
)
display(fig)

record(fig, plotsdir("learner", "anim", savename("predicted_trajectory", learning_params, "mkv")), enumerate(results_for_Cs), framerate=8, px_per_unit=3.0) do (index, result_pair)
    @show index

    data_driven_problem = data_driven_dataset.probs[index]

    for (trajectory_index, trajectory) in enumerate(predicted_trajectories)
        trajectory[] = result_pair.prediction[:, trajectory_index]
    end
    for (trajectory_index, trajectory) in enumerate(original_trajectories)
        trajectory[] = data_driven_problem.X[trajectory_index, :]
    end

    phase_space_function[] = result_pair.functions
    time_ticks[] = data_driven_problem.t

    C_value = data_driven_problem.p[1]
    Iext_value = data_driven_problem.p[2]
    axes[1].title = latexstring("\\text{\\textbf{Predicted trajectory}}, C = " * string(C_value) * ", I_e = " * string(Iext_value))

    for (axis_index, axis) in enumerate(axes)
        xlabel = to_string(:t) * sprintf(", true \$f_t=%2.3f\$ Hz, predicted \$f_p=%2.3f\$ Hz",
            result_pair.frequencies[axis_index], result_pair.ML_frequencies[axis_index])

        axis.xlabel = LaTeXString(xlabel)

        autolimits!(axis)
    end
end

x_bounds = -70 .. 110
y_bounds = -0.2 .. 0.4
start_index = 1
steps = 450

include(srcdir("anim.jl"))
value_space_split = (vec_of_mtr) -> [getindex.(vec_of_mtr, 1), getindex.(vec_of_mtr, 2)]

original_traj = getindex.(results_for_Cs, :original)
prediction_traj = getindex.(results_for_Cs, :prediction)
funcs = getindex.(results_for_Cs, :functions)
@time pred_val_space = (calculate_value_space(result_pair.functions, x_bounds, y_bounds; steps) for result_pair in results_for_Cs)
@time ML_val_space = (calculate_value_space(result_pair.ML_functions, x_bounds, y_bounds; steps) for result_pair in results_for_Cs)
titles = (latexstring("\\text{\\textbf{Polynomial phase space}}, C = " * string(result_pair.C_value)) for result_pair in results_for_Cs)

animate(
    plotsdir("learner", "anim", savename("custom_phase_space", learning_params, "gif")),
	original_traj,
	prediction_traj,
	funcs,
	pred_val_space,
	ML_val_space,
	titles
) do original_trajectory, predicted_trajectory, phase_space_function, f_obs, ML_obs, title, kwargs
    f₁_obs, f₂_obs = f_obs
    V_obs, n_obs = ML_obs

    fig, _ = plot_found_phase_space(original_trajectory, predicted_trajectory,
        x_bounds, y_bounds, phase_space_function,
        f₁_obs, f₂_obs, V_obs, n_obs;
        xlabel=L"V", ylabel=L"n",
        legend_outside=true, maxsteps=60,
        native_label=L"\text{polynomial trajectory}",
        transform_label=L"\text{Morris-Lecar trajectory}",
        title,
        resolution=(450, 600),
        contour_colors=[:cyan, :orchid1, :teal, :purple],
        level=0, steps,
		kwargs...
    )

    return fig
end


titles = (
    latexstring("\\text{\\textbf{Predicted trajectory}}, C = " * string(result_pair.C_value) * ", I_e = " * string(result_pair.Iext_value))
    for result_pair in results_for_Cs
)

animate(
    plotsdir("learner", "anim", savename("predicted_trajectory_anim", learning_params, "gif")),
    getindex.(results_for_Cs, :original),
    getindex.(results_for_Cs, :prediction),
    getindex.(results_for_Cs, :time_ticks),
    getindex.(results_for_Cs, :frequencies),
    getindex.(results_for_Cs, :ML_frequencies),
    titles
) do original_trajectory, predicted_trajectory, time_ticks, predicted_frequencies, true_frequencies, title
    fig, _ = each_predicted_trajectory_comparison_plot(time_ticks,
        predicted_trajectories, original_trajectories, [:V, :n];
        resolution=(550, 350), adjust_length=false, observables=true,
        title,
        predicted_frequencies,
        true_frequencies
    )

    return fig
end

