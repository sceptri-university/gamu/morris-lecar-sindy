using DrWatson
@quickactivate "Morris-Lecar SINDy"

using Revise
using CairoMakie
using Optim

RealOrAbsVector = Union{Real,AbstractVector}
IntOrAbsVector = Union{Integer,AbstractVector}

try_get(m, cond, func=x -> x + 1) = cond <= 0 ? 0 : func(m)

bern2vec(b) = reduce(append!, b)
function vec2bern(v::Vector{T}, M) where {T<:Real}
    b = Vector{T}[]
    csum = 0

    for m in M
        csumₒ = copy(csum) + 1
        csum += m + 1
        push!(b, v[csumₒ:csum])
    end

    return b
end

bern_length(M) = sum(m + 1 for m in M)

function bernstein_m(m::Integer, x::Real, M::Integer)::Real
    return binomial(M, m) * x^m * (1 - x)^(M - m)
end

function bernstein(x::T, M::S; to_vec=false, adjust_M = true) where {T<:RealOrAbsVector,S<:IntOrAbsVector}
	if typeof(x) <: AbstractVector && typeof(M) <: Real && adjust_M
		M = M .* ones(x)
	end

    @assert length(x) == length(M) "x and M must be the same length"
    @assert all(0 .<= x .<= 1) "x must be between 0 and 1"

    b = [[
        bernstein_m(m, x[i], Mᵢ)
        for m in 0:Mᵢ
    ] for (i, Mᵢ) in enumerate(M)
    ]

    return to_vec ? bern2vec(b) : b
end


function bernstein(x::T, M::S, θ::TT; adjust_M = true, kwargs...) where {T<:RealOrAbsVector,S<:IntOrAbsVector,TT<:AbstractVector}
    if typeof(x) <: AbstractVector && typeof(M) <: Real && adjust_M
        M = M .* ones(x)
    end

    @assert bern_length(M) == length(θ) "number coefficients θ must correspond to Ms"

    b = bernstein(x, M; kwargs...)
    return bern2vec(b)' * θ
end

M = [60,60]
xx = 0.1:0.01:1

rosenbrock(x,y) = ((1.0 - x)^2 + 100.0 * (y - x^2)^2)
i_rosenbrock(x,y) = begin
	u = 4*x - 2; v = 4*y -1
	return rosenbrock(u,v)
end

himmelblau(x, y) = (x^2 + y - 11)^2 + (x + y^2 - 7)^2
i_himmelblau(x, y) = begin
	u = 12*x - 6; v = 12*y - 6
	return himmelblau(u,v)
end

data = i_himmelblau.(xx, xx')
data2vec(data) = reduce(hcat, data)

IC_grid = [[x,y] for x in range(0.1, 1, M[1]+1), y in range(0.1, 1, M[2] + 1)]
guess_θ = [test_function(coords...) for coords in IC_grid]

θᵢ = ones(bern_length(M))

L₂(truth, prediction) = sum((truth .- prediction).^2)
function loss(θ, M, grid, truth)
	b(x) = bernstein(x, M, θ)
	prediction = [b([x,y]) for x in grid, y in grid]
	return L₂(data2vec(truth), data2vec(prediction))
end

@time result = optimize(θ -> loss(θ, M, xx, data), θᵢ)
θᵣ = result.minimizer

approximation = [bernstein([x,y], M, θᵣ) for x in xx, y in xx]

fig = Figure(; resolution=(700, 450))
ax1 = Axis(fig[1, 1]; title = "True Himmelblau")
contour!(ax1, xx, xx, i_himmelblau.(xx, xx'); levels=30, labels=true)
ax2 = Axis(fig[1, 2]; title = "Bernstein estimation with M = $M")
contour!(ax2, xx, xx, approximation; levels=30, labels=true)
fig

params = @dict M xx θᵢ
safesave(plotsdir("bernstein", savename("himmelblau", params, "pdf")), fig)

